package com.voicechanger.soundeffect.soundchanger.basseffect;

public interface IDBMediaListener {
    void onMediaCompletion();

    void onMediaError();
}
