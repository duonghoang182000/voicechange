package com.voicechanger.soundeffect.soundchanger.constants;

public interface IVoiceChangerConstants {
    public static final String AUDIO_RECORDER_FILE_EXT_MP3 = ".mp3";
    public static final String AUDIO_RECORDER_FILE_EXT_WAV = ".wav";
    public static final String AUDIO_RECORDER_FILE_EXT_M4A = ".m4a";
    public static final String FORMAT_NAME_VOICE = "voice_%1$s.wav";
    public static final String KEY_PATH_AUDIO = "KEY_PATH_AUDIO";
    public static final int RECORDER_SAMPLE_RATE = 44100;
}
